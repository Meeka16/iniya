﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	private GameObject startScreen;
	private Text startText;
	private GameObject startImage;
	private int secondsUntilLevelStart = 1;
	private bool settingUpGame;
	private int currentLevel = 1;
	public int waitForSeconds = 1;


	void Awake ()
	{
		if (Instance != null && Instance != this) 
		{
			DestroyImmediate(gameObject);
			return;
		}
		Instance = this;
	}


	void Start () {
		startScreen = GameObject.Find ("Start");
		InitializeGame();
		if (currentLevel != 1) {
			startImage = GameObject.Find("StartImage");
			startImage.SetActive (false);
		}
	}
	

	private void InitializeGame() {
		startImage = GameObject.Find ("Start Image");
		startText = GameObject.Find ("Start Text").GetComponent<Text>();
		startText.text = "Creepy-Looking Intergalctic Bird";
		startImage.SetActive (true);
		Invoke("DisableStartImage", secondsUntilLevelStart);
	}

	private void DisableStartImage()
	{
		startImage.SetActive(false);
		settingUpGame = true;
	}

	public void GameOver()
	{
		startText.text = "You Died Like Iniya's Hopes and Dreams!";
		startImage.SetActive(true);
		enabled = false;

	}

}