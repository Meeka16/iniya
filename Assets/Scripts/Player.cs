﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {


	public static GameController Instance;
    public Text healthText;
	public AudioClip coinSound1;
	public AudioClip coinSound2;

	private int playerHealth = 50;
	private int healthPerCoin = 5;
	private int healthPerEnemy = 10;
	private int secondsUntilNextLevel = 2;


	public float mSpeed;


	void Start () 
	{
		mSpeed = 7f;
		healthText.text = "Health: " + playerHealth;

	}

	

	void Update () 
	{   
		CheckIfGameOver ();
		transform.Translate (mSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, mSpeed * Input.GetAxis ("Vertical") * Time.deltaTime);

		if (Input.GetKeyDown ("space")) 
		{
		transform.Translate (Vector3.up * 260 * Time.deltaTime, Space.World);
		}



	} 

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith) {
	{
			if (objectPlayerCollidedWith.tag == "Asteroid")
			{
				Invoke("LoadNewLevel", secondsUntilNextLevel);
				enabled = false;
			}
		            else if (objectPlayerCollidedWith.tag == "Coin") 
		            {
			         playerHealth += healthPerCoin;
			         healthText.text = "+" + healthPerCoin + " Health\n" + "Health: " + playerHealth;
			         objectPlayerCollidedWith.gameObject.SetActive(false);
		            }
		
			        else if (objectPlayerCollidedWith.tag == "Enemy") 
			        {
				    playerHealth -= healthPerEnemy;
				    healthText.text = "-" + healthPerEnemy + " Health\n" + "Health: " + playerHealth;
				    objectPlayerCollidedWith.gameObject.SetActive(true);
				
			        }
			    
		
	} 

	}

	private void LoadNewLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
	


	private void CheckIfGameOver() 
	{
		if (playerHealth <= 0) 
		{
			GameController.Instance.GameOver();
		}
	}
}




	